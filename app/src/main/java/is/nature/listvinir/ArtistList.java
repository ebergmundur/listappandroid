package is.nature.listvinir;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.MalformedURLException;


/**
 * Created by eberg on 9.8.16.
 */
public class ArtistList extends AppCompatActivity {

    private static final String TAG = "ArtistList";

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;


    public static final List<ArtistItem> ITEMS = new ArrayList<ArtistItem>();
    public static final Map<String, ArtistItem> ITEM_MAP = new HashMap<String, ArtistItem>();

    private static final int COUNT = 100;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        //Make call to AsyncTask
        Log.v(TAG, "onCreate");
        new AsyncFetch().execute();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    static {
//    {
        // Add some sample items.
//        Log.v(TAG, "KICKER");
//
//        for (int i = 80; i <= COUNT; i++) {
//            addItem(createArtistItem(i));
//        }
        new AsyncFetch().execute();
    }

    private static void addItem(ArtistItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
//        Log.v(TAG, "ITEM " + item.id);
    }

//    private static ArtistItem createArtistItem(int position) {
//        return new ArtistItem(String.valueOf(position), "Item " + position, makeDetails(position));
//    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ArtistList Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://is.nature.listvinir/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ArtistList Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://is.nature.listvinir/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /**
     */
//    public static class ArtistItem {
//        public final String id;
//        public final String content;
//        public final String details;
//
//        public ArtistItem(String id, String content, String details) {
//            this.id = id;
//            this.content = content;
//            this.details = details;
//        }
//
//        @Override
//        public String toString() {
//            return content;
//        }
//    }


    public static class ArtWork {
        public String nafn_is;
        public String nafn_en;
        public String description_is;
        public String description_en;
        public String mediatype;
        public String art_file_url;
        public String art_image_url;
        public Date year;
    }


    public static class Personhome {
        public String house_name;
        public String sub_name_is;
        public String sub_name_en;
        public String description_is;
        public String description_en;
        public Date year_from;
        public Date year_to;
        public String home_image__url;
        public LatLng locPoint;
    }

    public static class ArtistItem {
        public String id;
        public Date modified;
        public String nafn;
        public String slug;
        public Date born;
        public Date deceased;
        public String title_is;
        public String title_en;
        public String alias_is;
        public String alias_en;
        public String intro_is;
        public String intro_en;
        public String detail_portrait;
        public String list_portrait;
        public String portrait_smallsquare;
        public String portrait_bigsquare;
        public String portrait_wide;
        public String detail_caption_is;
        public String detail_caption_en;
        public String detail_author;
        public String intro_read_is;
        public String intro_read_en;
        public List<ArtWork> arts = new ArrayList<ArtWork>();
        public List<Personhome> homes = new ArrayList<Personhome>();
    }

    static class AsyncFetch extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection conn;
        URL url = null;


//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
//
//        }

        @Override
        protected String doInBackground(String... params) {
//            Log.v(TAG, "doInBackground");
            try {

                // Enter URL address where your json file resides
                // Even you can make call to php file which returns json data
//                url = new URL("http://listapp.natturan.is/persons/");
                url = new URL("http://192.168.1.223:8000/static/listamenn/persons.json");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return e.toString();
            }
            try {
//                Log.v(TAG, "Setup HttpURLConnection");
                // Setup HttpURLConnection class to send and receive data from php and mysql
//                HttpURLConnection conn;
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);

                // setDoOutput to true as we recieve data from json file
//                conn.setDoOutput(true);

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return e1.toString();
            }

            try {
                conn.setRequestMethod("GET");
                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

//            pdLoading.dismiss();
            List<ArtistItem> data = new ArrayList<>();

//            pdLoading.dismiss();
            try {

                JSONArray jArray = new JSONArray(result);
//                Log.v(TAG, jArray.toString());

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    ArtistItem artistItem = new ArtistItem();

                    String dateModified = json_data.getString("modified");
                    String dateBorn = json_data.getString("born");
                    String dateDeceased = json_data.getString("deceased");
                    SimpleDateFormat sdflong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSZ");
                    SimpleDateFormat sdfshort = new SimpleDateFormat("yyyy-MM-dd");


                    Date modified = null;
                    if (dateModified != null){
                        try {
                            modified = sdflong.parse(dateModified);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    Date deceased = null;
                    if (dateBorn != null){
                        try {
                            deceased = sdfshort.parse(dateBorn);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    Date born = null;
                    if (dateDeceased != null){
                        try {
                            born = sdfshort.parse(dateDeceased);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }


                    artistItem.id = json_data.getString("id");
                    artistItem.modified = modified;
                    artistItem.nafn = json_data.getString("nafn");
                    artistItem.slug = json_data.getString("slug");
                    artistItem.born = born;
                    artistItem.deceased = deceased;
                    artistItem.title_is = json_data.getString("title_is");
                    artistItem.title_en = json_data.getString("title_en");
                    artistItem.alias_is = json_data.getString("alias_is");
                    artistItem.alias_en = json_data.getString("alias_en");
                    artistItem.intro_is = json_data.getString("intro_is");
                    artistItem.intro_en = json_data.getString("intro_en");
                    artistItem.detail_portrait = json_data.getString("detail_portrait");
                    artistItem.list_portrait = json_data.getString("list_portrait");
                    artistItem.portrait_smallsquare = json_data.getString("portrait_smallsquare");
                    artistItem.portrait_bigsquare = json_data.getString("portrait_bigsquare");
                    artistItem.portrait_wide = json_data.getString("portrait_wide");
                    artistItem.detail_caption_is = json_data.getString("detail_caption_is");
                    artistItem.detail_caption_en = json_data.getString("detail_caption_en");
                    artistItem.detail_author = json_data.getString("detail_author");
                    artistItem.intro_read_is = json_data.getString("intro_read_is");
                    artistItem.intro_read_en = json_data.getString("intro_read_en");

                    JSONArray phomes = json_data.getJSONArray("personhome_set");
                    for (int ii = 0; ii < phomes.length(); ii++) {
                        JSONObject ph_data = phomes.getJSONObject(ii);
                        if (ph_data != null){


                            Personhome ph = new Personhome();

                            JSONObject house = ph_data.getJSONObject("house");
                            JSONObject homeProp = house.getJSONObject("properties");
                            JSONObject homeGeo = house.getJSONObject("geometry");


                            String fromYear = ph_data.getString("year_from");
                            String toYear = ph_data.getString("year_to");
                            SimpleDateFormat home_sdfshort = new SimpleDateFormat("yyyy-MM-dd");


                            Date yf = null;
                            if (fromYear != null){
                                try {
                                    yf = home_sdfshort.parse(fromYear);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            Date yt = null;
                            if (toYear != null){
                                try {
                                    yt = home_sdfshort.parse(toYear);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            JSONArray latlong = homeGeo.getJSONArray("coordinates");
                            LatLng homePoint = new LatLng(latlong.getDouble(0), latlong.getDouble(1));

                            ph.house_name = homeProp.getString("name");
                            ph.sub_name_is = homeProp.optString("sub_name_is");
                            ph.sub_name_en = homeProp.optString("sub_name_en","");
                            ph.home_image__url = ph_data.optString("home_image__url");
                            ph.sub_name_en = ph_data.optString("sub_name_en");
                            ph.sub_name_en = ph_data.optString("sub_name_en");
                            ph.year_from = yf;
                            ph.year_from = yt;
                            ph.locPoint = homePoint;

                            artistItem.homes.add(ph);

                        }
                    }

                    JSONArray artworks = json_data.getJSONArray("art_set");
                    for (int iii = 0; iii < artworks.length(); iii++) {
                        JSONObject art_data = artworks.getJSONObject(iii);
                        if (art_data != null){


                            ArtWork aw = new ArtWork();

                            String artYear = art_data.getString("year");
                            SimpleDateFormat art_sdfshort = new SimpleDateFormat("yyyy-MM-dd");


                            Date artmade = null;
                            if (artYear != null){
                                try {
                                    artmade = art_sdfshort.parse(artYear);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }


                            aw.nafn_is = art_data.getString("nafn_is");
                            aw.nafn_en = art_data.getString("nafn_en");
                            aw.description_is = art_data.getString("description_is");
                            aw.description_en = art_data.getString("description_en");
                            aw.mediatype = art_data.getString("mediatype");
                            aw.art_file_url = art_data.getString("art_file_url");
                            aw.art_image_url = art_data.getString("art_image_url");
                            aw.year = artmade;

                            artistItem.arts.add(aw);
                        }
                    }


//                    data.add(artistItem);
                    addItem(artistItem);
                }

                // Setup and Handover data to recyclerview
//                mRVFishPrice = (RecyclerView)findViewById(R.id.fishPriceList);
//                mAdapter = new AdapterFish(MainActivity.this, data);
//                mRVFishPrice.setAdapter(mAdapter);
//                mRVFishPrice.setLayoutManager(new LinearLayoutManager(MainActivity.this));

            } catch (JSONException e) {
                Log.v(TAG, "JSONException " + e );
//                Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

}
