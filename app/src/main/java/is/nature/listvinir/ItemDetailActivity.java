package is.nature.listvinir;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.widget.MediaController.MediaPlayerControl;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemListActivity}.
 */


import is.nature.listvinir.MapsActivity;

public class ItemDetailActivity extends AppCompatActivity   {
    public static MediaPlayer mMediaPlayer = new MediaPlayer();
    public static Handler mHandler = new Handler();
    public static double timeElapsed = 0, finalTime = 0;
    public static SeekBar seekbar;
    public static TextView duration;
    private static final String ACTION_PLAY = "com.example.action.PLAY";

    private static final String TAG = "ItemDetailActivity";

    private SectionsPagerAdapter mSectionsPagerAdapter;

    public static AudioManager mAudioManager;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static ArtistList.ArtistItem mItem;
    public static String ARG_ITEM_ID = "item_id";


    ExecutorService threadPoolExecutor = Executors.newSingleThreadExecutor();


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        seekbar = ((SeekBar) findViewById(R.id.seekBar));
        duration = (TextView) findViewById(R.id.duration);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
//        mViewPager = (ViewPager) findViewById(R.id.container);
//        mViewPager = (ViewPager) findViewById(R.id.item_detail_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        //Log.v(TAG, "onCreate " + ARG_ITEM_ID );


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
//        if (savedInstanceState == null) {
//            // Create the detail fragment and add it to the activity
//            // using a fragment transaction.
//            Bundle arguments = new Bundle();
//            arguments.putString(ItemDetailFragment.ARG_ITEM_ID,
//                    getIntent().getStringExtra(ItemDetailFragment.ARG_ITEM_ID));
//            ItemDetailFragment fragment = new ItemDetailFragment();
//            fragment.setArguments(arguments);
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.item_detail_container, fragment)
//                    .commit();
//        }

//        Log.v(TAG, "onCreate");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            navigateUpTo(new Intent(this, ItemListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        updateSeekBarTimeFuture.cancel(true);
        if (mMediaPlayer != null){
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
    }


    // play mp3 song
    public void play(View view) {
        ((SeekBar) findViewById(R.id.seekBar)).setMax((int)  mMediaPlayer.getDuration());
        mMediaPlayer.start();
//        Log.v(TAG, "play " );
        timeElapsed = mMediaPlayer.getCurrentPosition();
//        Log.v(TAG, "play " + timeElapsed );
//        seekbar.setProgress((int) timeElapsed);
        mHandler.postDelayed(updateSeekBarTime, 100);
    }

    //handler to change seekBarTime
    private Runnable updateSeekBarTime = new Runnable() {

        public void run() {
            try {
                if (mMediaPlayer != null){
                    if (mMediaPlayer.isPlaying()){
                        //get current position
                        timeElapsed = mMediaPlayer.getCurrentPosition();
                        //set seekbar progress
                        ((SeekBar) findViewById(R.id.seekBar)).setProgress((int) timeElapsed);
                        //set time remaing
                        double timeRemaining = finalTime - timeElapsed;
                        ((TextView) findViewById(R.id.duration)).setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining), TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining))));

                        //repeat yourself that again in 100 miliseconds
                        mHandler.postDelayed(this, 100);

                    }
                }
            } catch ( Exception e){
                Log.e(TAG, "updateSeekBarTime ERROR ", e );
            }
        }
    };
    Future updateSeekBarTimeFuture = threadPoolExecutor.submit(updateSeekBarTime);
    //http://stackoverflow.com/questions/9458097/android-how-do-i-stop-runnable

    // pause mp3 song
    public void pause(View view) {
        mMediaPlayer.pause();
//        Log.v(TAG, "pause " );
    }


//    public onStartCommand(Intent intent, int flags, int startId) {
//        Log.v(TAG, "onStartCommand " );
//        if (intent.getAction().equals(ACTION_PLAY)) {
////            mMediaPlayer = ... // initialize it here
//            mMediaPlayer.setOnPreparedListener(this);
//            mMediaPlayer.prepareAsync(); // prepare async to not block main thread
//        }
//    }

//    public void onPrepared(MediaPlayer player) {
//        Log.v(TAG, "onPrepared " );
//        player.start();
//    }

/*    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ItemDetail Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://is.nature.listvinir/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ItemDetail Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://is.nature.listvinir/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }*/


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment  {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        public static  String ARG_ITEM_ID = "item_id";

        //        ThingsAdapter adapter;
        FragmentActivity listener;

        public PlaceholderFragment() {
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof Activity){
                this.listener = (FragmentActivity) context;
            }
            String itemId = getArguments().getString(ARG_ITEM_ID);
//            Log.v(TAG, "onAttach " + itemId );
        }
        public void setItemId(String itemId){
            ARG_ITEM_ID = itemId;
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            args.putString(ARG_ITEM_ID, "4" );
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Integer sect = getArguments().getInt(ARG_SECTION_NUMBER);
//            String itemId = getArguments().getString(ARG_ITEM_ID);
//
//            Log.v(TAG, "onCreateView " + sect );
//            Log.v(TAG, "itemId " + itemId );

//            if (getArguments().containsKey(ARG_ITEM_ID)) {
////            if (getArguments().containsKey(ARG_ITEM_ID)) {
////                mItem = ArtistList.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
////                mItem = ArtistList.ITEM_MAP.get("4");
//                Log.v(TAG, "mItem " + mItem.content );
//
//            Activity activity = this.getActivity();
//            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
//            if (appBarLayout != null) {
//                appBarLayout.setTitle(mItem.content);
//            }
//            }


            if (sect == 1) {
                View agripView = inflater.inflate(R.layout.agrip, container, false);
                if (mItem != null) {
                    ((TextView) agripView.findViewById(R.id.NameTitle)).setText(mItem.nafn);
                    ((TextView) agripView.findViewById(R.id.intro_text)).setText(mItem.intro_is);
                    ((TextView) agripView.findViewById(R.id.subText)).setText(mItem.title_is);
                    ImageView mImage = (ImageView) agripView.findViewById(R.id.mainImage);
                    String imgurl = "http://192.168.1.223:8000/media/appimages/"+mItem.portrait_wide;
                    new DownLoadImageTask(mImage).execute(imgurl);
//                ((ImageView) agripView.findViewById(R.id.mainImage)).setI("http://192.168.1.208:8000"+mItem.portrait_wide);
                }
                return agripView;
            }
            else if (sect == 3){
                final View artView = inflater.inflate(R.layout.verk, container, false);
                if (mItem != null) {
                    List<ArtistList.ArtWork> artItems = mItem.arts;
                    ArtistList.ArtWork artItem = artItems.get(0);

                    ((TextView) artView.findViewById(R.id.ArtTitle)).setText(artItem.nafn_is);
                    ((TextView) artView.findViewById(R.id.artText)).setText(artItem.description_is);
                    ((TextView) artView.findViewById(R.id.subText)).setText(mItem.title_is);
                    ImageView artImage = (ImageView) artView.findViewById(R.id.artImage);
                    String imgurl = "http://192.168.1.223:8000"+artItem.art_image_url;
                    new DownLoadImageTask(artImage).execute(imgurl);


//                    Log.v(TAG,"ART IS ON");
//                    Log.v(TAG,"ART TYPE "+ artItem.mediatype );
                    if (artItem.mediatype.equals("a")){
//                        Log.v(TAG,"ART IS AUDIO");
                        //  setController();
//                        String audiourl = "http://192.168.1.223:8000"+artItem.art_file_url;
                        artView.findViewById(R.id.playerControls).setVisibility(View.VISIBLE);


                        String audioFile = "http://192.168.1.223:8000/media/thulan.mp3" ;

                        try {
//                            mMediaPlayer.stop();
                            mMediaPlayer.release();

                            mMediaPlayer = new MediaPlayer();

                            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mMediaPlayer.setDataSource(audioFile);
                            mMediaPlayer.prepare();
                            finalTime = mMediaPlayer.getDuration();
                        } catch (IOException e) {
                            Log.e("PlayAudioDemo", "Could not open file " + audioFile + " for playback.", e);
                        }

//                        double timeElapsed = 0, finalTime = 0;

/*                        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mHandler.post(new Runnable() {
                                    public void run() {
//                                        ((TextView) artView.findViewById(R.id.currentPlayTime)).setText();
//                                        SeekBar seekbar = (SeekBar) artView.findViewById(R.id.seekBar);
//                                        seekbar.setMax((int) finalTime);
//                                        seekbar.setClickable(false);

//                                        mMediaPlayer.start();

//                                        Log.v(TAG,"PlayAudioDemo PLAYING? " +  mMediaPlayer.isPlaying() );
//                                        Log.v(TAG,"PlayAudioDemo RUNNING? ");
//                                        Log.v(TAG,"mAudioManager Mode B" + mAudioManager.getMode());
                                    }
                                });
                            }
                        });*/

                    } else {
                        artView.findViewById(R.id.playerControls).setVisibility(View.INVISIBLE);
                    }

                    //((MediaController) agripView.findViewById(R.id.mediaController)).


//                ((ImageView) agripView.findViewById(R.id.mainImage)).setI("http://192.168.1.208:8000"+mItem.portrait_wide);
                }
                return artView;
            }

            else {
                View homeView = inflater.inflate(R.layout.activity_maps, container, false);
//                TextView section_labelView = (TextView) rootView.findViewById(R.id.section_label);
//                section_labelView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

//                if (mItem != null) {
//                    ((TextView) rootView.findViewById(R.id.item_detail)).setText(mItem.details);
//                }
//


//                GoogleMap mMap = homeView.findViewById(R.id.map);

                FragmentManager fm = getChildFragmentManager();
                SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);

//                mapFragment.getMapAsync(GoogleMap googleMap);

//                View mMap = homeView.findViewWithTag("GoogleMapsView");
//
////                GoogleMap mMap;
////                mMap = ((GoogleMap) homeView.findViewById(R.id.map));
//                Log.v(TAG, "onMapReady "  );
//                // Add a marker in Sydney and move the camera
//                LatLng hvero = new LatLng(64.001401, -21.18397);
//                mMap.addMarker(new MarkerOptions().position(hvero).title("Hveragerði"));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(hvero));

                return homeView;
            }


//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            View agripView = inflater.inflate(R.layout.agrip, container, false);
//        View rootView = inflater.inflate(R.layout.item_detail, container, false);

//            Log.v(TAG, "mItem " + mItem.content);

            // Show the content as text in a TextView.


//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            // Setup any handles to view objects here
            // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

            String itemId = getArguments().getString(ARG_ITEM_ID);

//            Log.v(TAG, "onViewCreated itemId " + itemId );
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            String itemId = getArguments().getString(ARG_ITEM_ID);

//            Log.v(TAG, "onActivityCreated itemId " + itemId );
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Ágrip";
                case 1:
                    return "Heimili";
                case 2:
                    return "Verk";
            }
            return null;
        }
    }


//    static class DownloadFile extends AsyncTask<String, Integer, String>{
//        @Override
//        protected String doInBackground(String... url) {
//            int count;
//
//            Log.v(TAG, "DownloadFile  " );
//            try {
//                URL audioUrl = new URL("http://192.168.1.223:8000/media/filer_public/fd/0d/fd0dbd0b-cf62-45a3-a729-cb257697c7ac/helgi_songur.m4a");
//                URLConnection conexion = audioUrl.openConnection();
//                conexion.connect();
//                // this will be useful so that you can show a tipical 0-100% progress bar
//                int lenghtOfFile = conexion.getContentLength();
//
//                // downlod the file
//                InputStream input = new BufferedInputStream(audioUrl.openStream());
//
//
//               OutputStream output = new FileOutputStream("/sdcard/somewhere/nameofthefile.mp3");
//
//                byte data[] = new byte[1024];
//
//                long total = 0;
//
//                while ((count = input.read(data)) != -1) {
//                    total += count;
//                    // publishing the progress....
//                    publishProgress((int)(total*100/lenghtOfFile));
//                    output.write(data, 0, count);
//                    Log.v(TAG, "output  " + (int)(total*100/lenghtOfFile) );
//                }
//
//
//                try {
//                    mMediaPlayer.setDataSource("/sdcard/somewhere/nameofthefile.mp3");
//                    mMediaPlayer.prepare();
//                } catch (IOException e) {
//                    Log.e("PlayAudioDemo", "Could not open file " + input + " for playback.", e);
//                }
//
//                output.flush();
//                output.close();
//                input.close();
//            } catch (Exception e) {}
//            return null;
//        }
//    }

    static class DownLoadImageTask extends AsyncTask<String,Void,Bitmap>{
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;
//            Log.v(TAG, "DownLoadImageTask  " );
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String... urls){

            //Log.v(TAG, "urls  " + urls[0] );


            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
                logo = BitmapFactory.decodeStream(is);
                //Log.v(TAG, "logo  " + logo.toString() );
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */
        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }
}
